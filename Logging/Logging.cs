﻿using System;
using System.IO;
using System.Linq;

/// <summary>
/// A simple class for logging.
/// </summary>
public class Logging
{
    private static Logging _default;
    public static Logging Default => _default ?? (_default = new Logging("Log"));
    /// <summary>
    /// The name of the Logfile. Use [DATE] to specify where the DateTime should be.<para />Example: Log_[DATE].txt
    /// </summary>
    public string FileName { get; set; }
    /// <summary>
    /// The path where the Logfile will be located. Default if null. <para /> Default: Directory.GetCurrentDirectory()
    /// </summary>
    public string Path { get; set; }
    /// <summary>
    /// Specify the format of the DateTime used for the Filename. (will replace [DATE])<para />
    /// Also sets the Intevall of Logging.<para />
    /// Example: The default value is "yyyyMMdd" which will log daily. "yyyyMMdd_HH" will log hourly.
    /// </summary>
    public string DateTimeFormat { get; set; }
    /// <summary>
    /// Returns a strong if the current time in the format of DateTimeFormat
    /// </summary>
    private string DateTimeString => DateTime.Now.ToString(DateTimeFormat); 
    /// <summary>
    /// Specify the format of the DateTime in the Log.<para />Default: "dd.MM HH:mm:ss"
    /// </summary>
    public string LogTimeFormat { get; set; }
    /// <summary>
    /// Returns a strong if the current time in the format of LogTimeFormat
    /// </summary>
    private string LogDateTimeString =>  DateTime.Now.ToString(LogTimeFormat);

    /// <summary>
    /// Creates a new instance of Logging. (supports *.txt and *.log files)
    /// </summary>
    /// <param name="fileName">The name of the Logfile. Use [DATA] to specify where the DateTime should be.<para />Example: Log_[DATE].txt</param>
    /// <param name="path">The path where the Logfile will be located. Default if null. <para /> Default: Directory.GetCurrentDirectory()</param>
    /// <param name="dateTimeFormat">
    ///     Specify the format of the DateTime used for the Filename. (will replace [DATE])<para />
    ///     Also sets the Intevall of Logging.<para />
    ///     Example: The default value is "yyyyMMdd" which will log daily. "yyyyMMdd_HH" will log hourly.
    /// </param>
    /// <param name="logTimeFormat">Specify the format of the DateTime in the Log.<para />Default: "dd.MM HH:mm:ss"</param>
    public Logging(string fileName, string path = null, string dateTimeFormat = "yyyyMMdd", string logTimeFormat = "dd.MM. HH:mm:ss")
    {
        FileName = fileName;
        Path = path ?? Directory.GetCurrentDirectory();
        DateTimeFormat = dateTimeFormat;
        LogTimeFormat = logTimeFormat;

        if (!FileName.EndsWith(".txt") && !FileName.EndsWith(".log"))
            FileName += ".log";
        if (!FileName.Contains("[DATE]"))
        {
            string first = String.Concat(FileName.Take(FileName.Length - 1 - FileName.Split('.').Last().Length));
            FileName = first + "_[DATE]." + FileName.Split('.').Last();
        }
    }

    /// <summary>
    /// Writes text to the Logfile.
    /// </summary>
    /// <param name="text">The text to be written</param>
    /// <returns></returns>
    public string Write(string text, string prefix = null)
    {
        var currentPath = System.IO.Path.Combine(Path, FileName.Replace("[DATE]", DateTimeString));
        if (string.IsNullOrWhiteSpace(prefix))
            text = string.Join("\t\t", LogDateTimeString, text);
        else
            text = string.Join("\t\t", LogDateTimeString, prefix, text);

        if (File.Exists(currentPath))
            using (var writer = new StreamWriter(currentPath, true, System.Text.Encoding.Default))
                writer.WriteLine(text);
        else
        {
            Directory.CreateDirectory(Path);
            using (var writer = new StreamWriter(currentPath, false, System.Text.Encoding.Default))
                writer.WriteLine(text);
        }

        return text;
    }

    public string Write(Exception ex)//, string prefix = null)
    {
        return Write(ex.Message + Environment.NewLine + ex.StackTrace, ex.GetType().ToString());
    }
}
